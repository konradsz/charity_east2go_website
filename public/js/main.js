var slider_video = {
    current: 0
};
var slider_tool = {
    current: 0
};

var tool_width = 200;
var tool_margin = 20;
var video_width = 400;
var video_margin = 0;
var upper_bound = null;
var lower_bound = null;
var nr, width, window_width = null;

function go_player(direction_int) {
    window_width = $(window).width();

    if(window_width <= 768) {
        video_width = 320;
    } else {
        video_width = 400;
    }
    go($('#video-row'), $('.video').length, slider_video, direction_int, video_width, video_margin, 768)
}

function go_tools(direction_int) {
    go($('#tools-row'), $('.tool').length, slider_tool, direction_int, tool_width, tool_margin, 450)
}

function go (row, arr_length, slider, direction_int, elem_width, elem_margin, mobile_width) {

    if(window_width >= 1024) {
        lower_bound = 2;
        upper_bound = 3;
    } else if(window_width <= mobile_width) {
        lower_bound = 0;
        upper_bound = 1;
    } else {
        // tablet
        lower_bound = 1;
        upper_bound = 2;
    }

    nr = slider.current + direction_int;

    if (arr_length === 0) {
        return false;
    }
    if(nr >= arr_length - lower_bound) {
        nr = 0;
    }
    if(nr < 0) {
        nr = arr_length - upper_bound;
    }
    slider.current = nr;

    width = nr * (elem_width + elem_margin);

    row.css('left', '-' + width + 'px');
}

document.addEventListener('DOMContentLoaded', function () {

    // Get all "navbar-burger" elements
    var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                var target = $el.dataset.target;
                var $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});

$(window).on( 'resize.topslider', function(){
    go_player(0);
});